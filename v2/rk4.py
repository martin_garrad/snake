import numpy as np
import matplotlib.pyplot as plt

def rk4(rhs, x0, times, args=None):

	t0 = times[0]

	y = np.zeros((len(times), len(x0)))
	y[0, :] = x0

	if args:
		args = args[0]

	for (i, t) in enumerate(times[1:]):
		dt = t - t0
		k1 = rhs(y[i, :], t, args)
		k2 = rhs(y[i, :]+(dt/2.*k1), t+dt/2., args)
		k3 = rhs(y[i, :]+(dt/2.*k2), t+dt/2., args)
		k4 = rhs(y[i, :]+dt * k3, t+dt, args)

		y[i+1, :] = y[i, :] + (dt/6.) * (k1 + 2*k2 + 2 * k3 + k4)
		t0 = t

	return y