#!/usr/bin/env python
import snake
import link
import spring
import numpy as np
import matplotlib
import wx
import wx.lib.scrolledpanel
import wx.lib.mixins.listctrl  as  listmix
from matplotlib.figure import Figure
from matplotlib.backends.backend_wxagg import \
    FigureCanvasWxAgg as FigCanvas, \
    NavigationToolbar2WxAgg as NavigationToolbar
import threading

DPI = 50
FIG_SIZE = (5.0, 5.0)

class MainWindow(wx.Frame):
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, title=title, size=(950, 725))

        self.sizer = wx.GridBagSizer()

        self.SnakeControl = SnakeControl(self)
        self.AnimPlot = PlotPanel(self)
        self.AnimControl = AnimControl(self)
        self.AnalysisPlot = PlotPanel(self)
        self.PlotControl = PlotControl(self)
        self.console = Console(self)

        self.sizer.Add(self.SnakeControl, (0, 0), span=(4, 1), flag=wx.EXPAND)
        self.sizer.Add(self.AnimPlot, (0, 1), flag=wx.GROW)
        self.sizer.Add(self.AnimControl, (1, 1), flag=wx.GROW)
        self.sizer.Add(self.AnalysisPlot, (2, 1), flag=wx.GROW)
        self.sizer.Add(self.PlotControl, (3, 1), flag=wx.GROW)
        self.sizer.Add(self.console, (4, 1), flag=wx.EXPAND)

        self.SetMinSize((600, 450))
        self.SetAutoLayout(True)
        self.SetSizer(self.sizer)
        self.Layout()
        self.Show()

class PlotPanel(wx.lib.scrolledpanel.ScrolledPanel):
    def __init__(self, parent):
        self.dpi = DPI
        self.fig = Figure(FIG_SIZE, dpi=self.dpi)
        wx.lib.scrolledpanel.ScrolledPanel.__init__(self, parent, style=wx.SIMPLE_BORDER ^ wx.RESIZE_BORDER)
        self.SetupScrolling()
        self.SetMinSize((650, 295))

        self.canvas = FigCanvas(self, -1, self.fig)
        self.toolbar = NavigationToolbar(self.canvas)
        self.toolbar.Realize()
        self.fig.canvas.mpl_connect('button_press_event', self.on_subplot_click)

        self.Bind(wx.EVT_SIZE, self.on_size)
        self.zoom_state = 0

        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(self.toolbar, 1, wx.EXPAND)
        self.sizer.Add(self.canvas, 0, wx.EXPAND)

        self.SetSizer(self.sizer)
        self.Fit()

    def on_size(self, event):
        pass

    def on_subplot_click(self, event):
        ax = event.inaxes
        if ax is None:
            # Occurs when a region not in an axis is clicked...
            return
        if event.button is 1:
            if self.zoom_state == 0:
            # On left click, zoom the selected axes
                ax._orig_position = ax.get_position()
                ax.set_position([0.1, 0.15, 0.85, 0.75])
                for axis in event.canvas.figure.axes:
                    # Hide all the other axes...
                    if axis is not ax:
                        axis.set_visible(False)
                self.zoom_state = 1
        elif event.button is 3:
            if self.zoom_state == 1:
            # On right click, restore the axes
                try:
                    ax.set_position(ax._orig_position)
                    for axis in event.canvas.figure.axes:
                        axis.set_visible(True)
                    self.zoom_state = 0
                except AttributeError:
                    # If we haven't zoomed, ignore...
                    pass
        else:
            # No need to re-draw the canvas if it's not a left or right click
            return
        event.canvas.draw()

class SnakeControl(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, style=wx.SIMPLE_BORDER)
        self.parent = parent

        self.sizer = self.sizer = wx.GridBagSizer()

        self.n_link_label = wx.StaticText(self, label="Number of links")
        self.timestep_label = wx.StaticText(self, label="Timestep")
        self.ep_length_label = wx.StaticText(self, label="Episode length")
        self.gravity_label = wx.StaticText(self, label="Gravity")
        self.friction_label = wx.StaticText(self, label="Joint Friction")

        self.n_link = wx.TextCtrl(self)
        self.n_link.SetValue("2")
        self.timestep = wx.TextCtrl(self)
        self.timestep.SetValue("0.01")
        self.episode_length = wx.TextCtrl(self)
        self.episode_length.SetValue("20")
        self.gravity_value = wx.TextCtrl(self)
        self.gravity_value.SetValue("-9.81")
        self.friction_value = wx.TextCtrl(self)
        self.friction_value.SetValue("0.1")

        self.make_snake_button = wx.Button(self, -1, "Make Snake")
        self.solve_button = wx.Button(self, -1, "Solve")

        self.Bind(wx.EVT_BUTTON, self.solve, self.solve_button)
        self.Bind(wx.EVT_BUTTON, self.make_snake, self.make_snake_button)

        self.sizer.Add(self.n_link_label, (0, 0), flag=wx.EXPAND)
        self.sizer.Add(self.n_link, (0, 1), flag=wx.EXPAND)
        self.sizer.Add(self.make_snake_button, (0, 2), flag=wx.EXPAND)
        self.sizer.Add(self.timestep_label, (1, 0), flag=wx.EXPAND)
        self.sizer.Add(self.timestep, (1, 1), flag=wx.EXPAND)
        self.sizer.Add(self.ep_length_label, (2, 0), flag=wx.EXPAND)
        self.sizer.Add(self.episode_length, (2, 1), flag=wx.EXPAND)
        self.sizer.Add(self.gravity_label, (3, 0), flag=wx.EXPAND)
        self.sizer.Add(self.gravity_value, (3, 1), flag=wx.EXPAND)
        self.sizer.Add(self.friction_label, (4, 0), flag=wx.EXPAND)
        self.sizer.Add(self.friction_value, (4, 1), flag=wx.EXPAND)
        self.sizer.Add(self.solve_button, (5, 0), flag=wx.EXPAND)

        self.SetSizer(self.sizer)
        self.make_snake(None)


    def solve(self, event):

        self.update_params() # Reads updated parameters from GUI interace
        self.snake.sub_params() #Substitutes values into EoM where necessary

        x0 = np.hstack(([ _ * 0.1 for _ in range(self.snake.n)], np.zeros(self.snake.n))) # Initial conditions, q and u
        ep_length = int(self.episode_length.GetValue())
        dt = float(self.timestep.GetValue())
        num_steps = ep_length / dt
        time = np.linspace(0, ep_length, num_steps)  # Time vector

        self.snake.solve(x0, time)

    def update_params(self):
        #Change link params
        self.parent.LinkControl.update_params()
        #Change spring params
        self.parent.SpringControl.update_params()
        #Change motor params
        self.parent.MotorControl.update_params()

    def make_snake(self, event):
        #Solves the equations of motion for a system with N links
        #Eventually this should use a cache to reduce loading times
        if hasattr(self, 'nb'):
            self.nb.Destroy()

        try:
            n = int(self.n_link.GetValue())
            self.snake = snake.Snake(n)
            self.snake.get_eom()
            self.links = self.snake.links
            self.springs = self.snake.springs
            self.motors = self.snake.motors

            self.nb =  wx.Notebook(self)
            self.parent.LinkControl = LinkControl(self.nb, self.links)
            self.parent.SpringControl = SpringControl(self.nb, self.springs)
            self.parent.MotorControl = MotorControl(self.nb, self.motors)

            self.nb.AddPage(self.parent.LinkControl, "Links")
            self.nb.AddPage(self.parent.SpringControl, "Springs")
            self.nb.AddPage(self.parent.MotorControl, "Motors")

            self.sizer.Add(self.nb, (6, 0), span=(3, 5), flag=wx.EXPAND)
            self.Fit()

        except Exception,e: 
            print str(e)


class PlotControl(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, style=wx.SIMPLE_BORDER)

        self.sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.parent = parent
        
        self.abs_state_button = wx.Button(self, -1, "Plot Absolute State")
        self.rel_state_button = wx.Button(self, -1, "Plot Relative State")
        self.phase_button = wx.Button(self, -1, "Plot Phase Diagram")

        self.Bind(wx.EVT_BUTTON, self.plot_abs_state, self.abs_state_button)
        self.Bind(wx.EVT_BUTTON, self.plot_rel_state, self.rel_state_button)
        self.Bind(wx.EVT_BUTTON, self.plot_phase, self.phase_button)

        self.sizer.Add(self.abs_state_button, flag=wx.EXPAND)
        self.sizer.Add(self.rel_state_button, flag=wx.EXPAND)
        self.sizer.Add(self.phase_button, flag=wx.EXPAND)

        self.SetSizer(self.sizer)
        self.Fit()

    def plot_abs_state(self, e):
        self.parent.SnakeControl.snake.plot_state(fig=self.parent.AnalysisPlot.fig)

    def plot_rel_state(self, e):
        self.parent.SnakeControl.snake.plot_relative_state(fig=self.parent.AnalysisPlot.fig)

    def plot_phase(self, e):
        self.parent.SnakeControl.snake.plot_phase(fig=self.parent.AnalysisPlot.fig)

class AnimControl(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, style=wx.SIMPLE_BORDER)
        self.sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.parent = parent

        self.plot_t_label = wx.StaticText(self, label="Time")
        self.plot_t = wx.TextCtrl(self)
        self.plot_t.SetValue("0")

        self.animate = wx.Button(self, -1, "Animate")
        self.plot_button = wx.Button(self, -1, "Plot")
        self.step_forward_button = wx.Button(self, -1, "Step forward")
        self.step_backward_button= wx.Button(self, -1, "Step backward")
        self.step_size_label = wx.StaticText(self, label="Step size")
        self.step_size = wx.TextCtrl(self)
        self.step_size.SetValue("1")

        self.Bind(wx.EVT_BUTTON, self.animate_snake, self.animate)
        self.Bind(wx.EVT_BUTTON, self.step_forward, self.step_forward_button)
        self.Bind(wx.EVT_BUTTON, self.step_backward, self.step_backward_button)
        self.Bind(wx.EVT_BUTTON, self.plot_snake, self.plot_button)

        self.sizer.Add(self.animate, flag=wx.EXPAND)
        self.sizer.Add(self.plot_button, flag=wx.EXPAND)
        self.sizer.Add(self.plot_t_label, flag=wx.EXPAND)
        self.sizer.Add(self.plot_t, flag=wx.EXPAND)
        self.sizer.Add(self.step_forward_button, flag=wx.EXPAND)
        self.sizer.Add(self.step_backward_button, flag=wx.EXPAND)
        self.sizer.Add(self.step_size_label, flag=wx.EXPAND)
        self.sizer.Add(self.step_size, flag=wx.EXPAND)

        self.SetSizer(self.sizer)
        self.Fit()

    def animate_snake(self, e):

        self.parent.SnakeControl.snake.animate_snake(len(self.parent.SnakeControl.snake.t))

    def plot_snake(self, e):
        self.parent.SnakeControl.snake.plot_snake(int(self.plot_t.GetValue()), fig=self.parent.AnimPlot.fig)

    def step_forward(self, e):
        time = int(self.plot_t.GetValue())
        step = int(self.step_size.GetValue())

        self.plot_t.SetValue(str(min(time+step, len(self.parent.SnakeControl.snake.t)-1)))
        self.plot_snake(None)

    def step_backward(self, e):
        time = int(self.plot_t.GetValue())
        step = int(self.step_size.GetValue())

        self.plot_t.SetValue(str(max(0, time-step)))
        self.plot_snake(None)



class LinkControl(wx.lib.scrolledpanel.ScrolledPanel):

    '''
    This panel is used to display and modify the parameters of the individual links
    The list control indices correspond to:
    0. Link number
    1. Mass
    2. Length
    3. Width
    4. Separation
    5. Parent 
    6. Child
    '''

    def __init__(self, parent, links):

        wx.lib.scrolledpanel.ScrolledPanel.__init__(self, parent, style=wx.SIMPLE_BORDER ^ wx.RESIZE_BORDER)
        self.SetupScrolling()
        self.SetMinSize((250, 200))
        self.sizer = wx.BoxSizer(wx.VERTICAL)

        self.links = links

        self.title = wx.StaticText(self, label="Links", style=wx.ALIGN_CENTRE_HORIZONTAL)

        rows = [(str(i), str(l.mass), str(l.length), str(l.width), str(l.separation), str(i-1), str(i+1)) for (i,l) in enumerate(self.links)]

        self.link_list = EditableListCtrl(self, style=wx.LC_REPORT)
 
        self.link_list.InsertColumn(0, "Number")
        self.link_list.InsertColumn(1, "Mass")
        self.link_list.InsertColumn(2, "Length")
        self.link_list.InsertColumn(3, "Width")
        self.link_list.InsertColumn(4, "Separation")
        self.link_list.InsertColumn(5, "Parent")
        self.link_list.InsertColumn(6, "Child")
 

        for (i, row) in enumerate(rows):
            self.link_list.InsertStringItem(i, row[0])
            self.link_list.SetStringItem(i, 1, row[1])
            self.link_list.SetStringItem(i, 2, row[2])
            self.link_list.SetStringItem(i, 3, row[3])
            self.link_list.SetStringItem(i, 4, row[4])
            self.link_list.SetStringItem(i, 5, row[5])
            self.link_list.SetStringItem(i, 6, row[6])

        self.sizer.Add(self.title, flag=wx.EXPAND)
        self.sizer.Add(self.link_list, 0, wx.ALL|wx.EXPAND, 5)

        self.SetSizer(self.sizer)
        self.Fit()

    def update_params(self):
        
        for (i, l) in enumerate(self.links):
            l.mass = float(self.link_list.GetItem(itemId=i, col=1).GetText())
            l.length = float(self.link_list.GetItem(itemId=i, col=2).GetText())
            l.width = float(self.link_list.GetItem(itemId=i, col=3).GetText())
            l.separation = float(self.link_list.GetItem(itemId=i, col=4).GetText())


class SpringControl(wx.lib.scrolledpanel.ScrolledPanel):

    '''
    This panel is used to display and modify the parameters of the individual springs
    The list control indices are determined by the spring type.
    Each spring is expected to have a params property which lists its parameters
    0. Link number
    1. Resting length
    2. Stiffness
    3+ .params
    '''

    def __init__(self, parent, springs):

        wx.lib.scrolledpanel.ScrolledPanel.__init__(self, parent, style=wx.SIMPLE_BORDER ^ wx.RESIZE_BORDER)
        self.SetupScrolling()
        self.SetMinSize((250, 100))
        self.sizer = wx.BoxSizer(wx.VERTICAL)

        self.springs= springs

        self.title = wx.StaticText(self, label="Springs", style=wx.ALIGN_CENTRE_HORIZONTAL)

        rows = [(str(i), str(s.resting_length), str(s.stiffness), str(s.type), str(s.top_link.name), str(s.bottom_link.name)) for (i,s) in enumerate(self.springs)]

        self.spring_list = EditableListCtrl(self, style=wx.LC_REPORT)
 
        self.spring_list.InsertColumn(0, "Number")
        self.spring_list.InsertColumn(1, "Resting Length")
        self.spring_list.InsertColumn(2, "Stiffness")
        self.spring_list.InsertColumn(3, "Type")
        self.spring_list.InsertColumn(4, "Top Link")
        self.spring_list.InsertColumn(5, "Bottom Link")

        try:
            for (i, _) in enumerate(self.springs[0].params):
                self.spring_list.InsertColumn(5+i, _)
        except:
            pass
 
        for (i, row) in enumerate(rows):
            self.spring_list.InsertStringItem(i, row[0])
            self.spring_list.SetStringItem(i, 1, row[1])
            self.spring_list.SetStringItem(i, 2, row[2])
            self.spring_list.SetStringItem(i, 3, row[3])
            self.spring_list.SetStringItem(i, 4, row[4])
            self.spring_list.SetStringItem(i, 5, row[5])

        self.sizer.Add(self.title, flag=wx.EXPAND)
        self.sizer.Add(self.spring_list, 0, wx.ALL|wx.EXPAND, 5)

        self.SetSizer(self.sizer)
        self.Fit()

    def update_params(self):
        
        for (i, s) in enumerate(self.springs):
            s.resting_length = float(self.spring_list.GetItem(itemId=i, col=1).GetText())
            s.stiffness = float(self.spring_list.GetItem(itemId=i, col=2).GetText())


class MotorControl(wx.lib.scrolledpanel.ScrolledPanel):
    '''
    This panel is used to display and modify the parameters of the motors
    It is also capable of plotting the reference signals of any feedback controllers
    Each motor is expected to have a params property which lists its parameters
    0. Link number
    1. Reference Signal
    2. Amplitude
    3. Frequency
    4. kp
    5. kd
    '''

    def __init__(self, parent, motors):

        wx.lib.scrolledpanel.ScrolledPanel.__init__(self, parent, style=wx.SIMPLE_BORDER ^ wx.RESIZE_BORDER)
        self.SetupScrolling()
        self.SetMinSize((250, 100))
        self.sizer = wx.BoxSizer(wx.VERTICAL)

        self.motors = motors

        self.title = wx.StaticText(self, label="Motors", style=wx.ALIGN_CENTRE_HORIZONTAL)

        rows = [(str(i), str(m.reference), str(m.amplitude), str(m.frequency), str(m.kp), str(m.kd)) for (i,m) in enumerate(self.motors) if m.type != "Const"]

        self.motor_list = EditableListCtrl(self, style=wx.LC_REPORT)
 
        self.motor_list.InsertColumn(0, "Number")
        self.motor_list.InsertColumn(1, "Reference")
        self.motor_list.InsertColumn(2, "amplitude")
        self.motor_list.InsertColumn(3, "Frequency")
        self.motor_list.InsertColumn(4, "KP")
        self.motor_list.InsertColumn(5, "KD")

 
        for (i, row) in enumerate(rows):
            self.motor_list.InsertStringItem(i, row[0])
            self.motor_list.SetStringItem(i, 1, row[1])
            self.motor_list.SetStringItem(i, 2, row[2])
            self.motor_list.SetStringItem(i, 3, row[3])
            self.motor_list.SetStringItem(i, 4, row[4])
            self.motor_list.SetStringItem(i, 5, row[5])

        self.sizer.Add(self.title, flag=wx.EXPAND)
        self.sizer.Add(self.motor_list, 0, wx.ALL|wx.EXPAND, 5)

        self.SetSizer(self.sizer)
        self.Fit()

    def update_params(self):

        for (i, m) in enumerate(self.motors):
            if m.type == "PID":
                m.amplitude = float(self.motor_list.GetItem(itemId=i, col=2).GetText())
                m.frequency = float(self.motor_list.GetItem(itemId=i, col=3).GetText())
                m.kp = float(self.motor_list.GetItem(itemId=i, col=2).GetText())
                m.kd = float(self.motor_list.GetItem(itemId=i, col=3).GetText())

class Console(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent, style=wx.SIMPLE_BORDER)
        self.out = wx.TextCtrl(self, -1, size=(650, 50), style = wx.TE_MULTILINE|wx.TE_READONLY|wx.HSCROLL|wx.EXPAND)

    def write(self, string):
        self.out.WriteText(string)

class EditableListCtrl(wx.ListCtrl, listmix.TextEditMixin):
    ''' TextEditMixin allows any column to be edited. '''
 
    #----------------------------------------------------------------------
    def __init__(self, parent, ID=wx.ID_ANY, pos=wx.DefaultPosition,
                 size=wx.DefaultSize, style=0):
        """Constructor"""
        wx.ListCtrl.__init__(self, parent, ID, pos, size, style)
        listmix.TextEditMixin.__init__(self)



def main():

    app = wx.App(False)
    frame = MainWindow(None, "Snake on the Plane")
    app.MainLoop()

if __name__ == "__main__":
    main()