'''
This class represents a spring pair.
Eventually this may be extended to make use of the springs used in the Msc Project
'''
import link
import matplotlib.pyplot as plt
import numpy as np
from numpy import sin, cos, pi

class Spring:

	def __init__(self, l1, l2, stiffness, params=dict()):

		self.stiffness = stiffness
		self.resting_length = 0.2
		self.type = "Paired"
		self.params = []

		self.top_link = l1
		self.bottom_link = l2

		for (k, v) in params.iteritems():
			setattr(self, k, v)

	def get_force_mag(self):
		#Returns the magnitude of the force vectors provided by each spring

		(l1, l2) = self.get_length()

		f1 = -self.stiffness * (l1 - self.resting_length)
		f2 = -self.stiffness * (l2 - self.resting_length)

		return (f1, f2)

	def get_vectors(self):
		#Returns the unit vectors describing the direction of the springs - Top to bottom is consider positive
		rs1 = self.top_link.get_corners()
		rs2 = self.bottom_link.get_corners()

		(l1, l2) = self.get_length()

		v1 = [(rs1[0][0] - rs2[3][0])/l1, (rs1[0][1]- rs2[3][1])/l1]
		v2 = [(rs1[1][0] - rs2[2][0])/l2, (rs1[1][1] - rs2[2][1])/l2]

		return (v1, v2)

	def get_force(self):
		#Returns the four force vectors due to the spring pair

		#Calculate forces
		(v1, v2) = self.get_vectors()
		(m1, m2) = self.get_force_mag()

		return [[m1*v1[0], m1*v1[1]], [m2*v2[0], m2*v2[1]], [-m1*v1[0], -m1*v1[1]], [-m2*v2[0], -m2*v2[1]]]

	def get_torque(self):
		
		#Returns the torques that each spring pair produces at the CoM of the connected links
		#A positive torque induces anti-clockwise rotation
		#f1 and f2 produces torques on the top link
		#f3 and f3 produce torques on the bottom link

		r1s = self.top_link.get_radial_vectors()
		r2s = self.bottom_link.get_radial_vectors()

		[f1, f2, f3, f4] = self.get_force()

		t1l = np.cross(r1s[0], f1)
		t1r = np.cross(r1s[1], f2)

		t2l = np.cross(r2s[3], f3)
		t2r = np.cross(r2s[2], f4)

		t1 =  -1 * (t1l + t1r)
		t2 = -1 * (t2l + t2r)

		return (t1, t2)

	def get_length(self):
		#Returns the lengths of the two springs

		rs1 = self.top_link.get_corners()
		rs2 = self.bottom_link.get_corners()

		l1 = np.sqrt((rs1[0][0] - rs2[3][0])**2 + (rs1[0][1] - rs2[3][1])**2)
		l2 = np.sqrt((rs1[1][0] - rs2[2][0])**2 + (rs1[1][1] - rs2[2][1])**2)

		return (l1, l2)

	def draw_force_vectors(self, ax):

		rs1 = self.top_link.get_corners()
		rs2 = self.bottom_link.get_corners()

		[f1, f2, f3, f4] = self.get_force()

		ax.plot([rs1[0][0], rs1[0][0] + f1[0]], [rs1[0][1], rs1[0][1] + f1[1]], 'cyan')
		ax.plot([rs1[1][0], rs1[1][0] + f2[0]], [rs1[1][1], rs1[1][1] + f2[1]], 'cyan')
		ax.plot([rs2[3][0], rs2[3][0] + f3[0]], [rs2[3][1], rs2[3][1] + f3[1]], 'cyan')
		ax.plot([rs2[2][0], rs2[2][0] + f4[0]], [rs2[2][1], rs2[2][1] + f4[1]], 'cyan')

	def draw(self, ax):
		#Draws the spring

		#Get Co-ordinates of both links in global reference frame			
		rs1 = self.top_link.get_corners()
		rs2 = self.bottom_link.get_corners()

		#Left spring
		#Attaches bottom left (r10) of top link to top left (r23) of top link
		ax.plot([rs1[0][0], rs2[3][0]], [rs1[0][1], rs2[3][1]], 'red')

		#Right spring
		#Attaches bottom right (r11) of top link to top right (r22) of bottom link
		ax.plot([rs1[1][0], rs2[2][0]], [rs1[1][1], rs2[2][1]], 'red')

class MACEPPA:

	'''
	This class represents a MACEPPA spring
	Changing the pretension allows the effective stiffness to be changed
	'''

	def __init__(self, l1, l2, stiffness, extension, pretension, params=dict()):

		self.stiffness = stiffness
		self.extension = extension
		self.pretension = pretension
		self.resting_length = l1.separation - self.extension
		print self.resting_length
		self.type = "MACEPPA"
		self.params = []

		self.top_link = l1
		self.bottom_link = l2

		for (k, v) in params.iteritems():
			setattr(self, k, v)

	def get_force_mag(self):
		#Returns the magnitude of the force vectors provided by the spring

		l =  self.get_length()

		f = -self.stiffness * ((l+self.pretension) - self.resting_length)

		#print "Resting length %f" % self.resting_length
		#print "Spring extension %f" % ((l+self.pretension) - self.resting_length)
		#print "Spring force: %f" % f

		return f

	def get_length(self):
		#Returns the lengths of the spring

		top_angle = self.top_link.angle * pi/180.
		bottom_angle = self.bottom_link.angle * pi/180.
		

		r1 = self.top_link.get_end_point()
		ex = [r1[0] + self.extension * sin(top_angle), r1[1] - self.extension * cos(top_angle)]
		r2 = [r1[0] + (self.bottom_link.separation * sin(bottom_angle)), r1[1] - (self.bottom_link.separation * cos(bottom_angle))]

		l = np.sqrt((ex[0] - r2[0])**2 + (ex[1]-r2[1])**2)
		#print "Spring length: %f" % l

		return l

	def get_vectors(self):
		#Returns the unit vectors describing the direction of the spring Top to bottom is consider positive
		top_angle = self.top_link.angle * pi/180.
		bottom_angle = self.bottom_link.angle * pi/180.
		

		r1 = self.top_link.get_end_point()
		ex = [r1[0] + self.extension * sin(top_angle), r1[1] - self.extension * cos(top_angle)]
		r2 = [r1[0] + (self.bottom_link.separation * sin(bottom_angle)), r1[1] - (self.bottom_link.separation * cos(bottom_angle))]

		l = self.get_length()

		v = [-(r2[0] - ex[0])/l, -(r2[1]- ex[1])/l]

		#print "Force vector: (%f,  %f)" % (v[0], v[1])

		return v

	def get_force(self):
		#Returns the force vectors due to the spring pair

		#Calculate forces
		v = self.get_vectors()
		m = self.get_force_mag()

		return [[-m*v[0], -m*v[1]], [m*v[0], m*v[1]]]

	def get_radial_vectors(self):

		top_angle = self.top_link.angle * pi/180.
		bottom_angle = self.bottom_link.angle * pi/180.

		r1 = [((self.top_link.length / 2.) + self.top_link.separation + self.extension) * sin(top_angle), -((self.top_link.length / 2.) + self.top_link.separation + self.extension) * cos(top_angle)]
		r2 = [-((self.bottom_link.length / 2.)) * sin(bottom_angle), ((self.bottom_link.length / 2.)) * cos(bottom_angle)]

		return (r1, r2)

	def get_torque(self):
		
		#Returns the torques that each spring pair produces at the CoM of the connected links
		#A positive torque induces anti-clockwise rotation

		(r1, r2) = self.get_radial_vectors()

		[f1, f2] = self.get_force()

		t1 = np.cross(r1, f1)
		#Second torque is negative as the radial vector is in the opposite direction
		t2 = np.cross(r2, f2)

		return (t1, -t2)


	def draw_force_vectors(self, ax):

		top_angle = self.top_link.angle * pi/180.
		bottom_angle = self.bottom_link.angle * pi/180.

		r1 = self.top_link.get_end_point()
		ex = [r1[0] + self.extension * sin(top_angle), r1[1] - self.extension * cos(top_angle)]
		#Draw extension
		ax.plot([r1[0], ex[0]], [r1[1], ex[1]], 'green')

		r2 = [r1[0] + (self.bottom_link.separation * sin(bottom_angle)), r1[1] - (self.bottom_link.separation * cos(bottom_angle))]

		[f1, f2] = self.get_force()

		ax.plot([ex[0], ex[0] - f1[0]], [ex[1], ex[1] - f1[1]], 'magenta')
		ax.plot([r2[0], r2[0] - f2[0]], [r2[1], r2[1] - f2[1]], 'cyan')

	def draw_radial_vectors(self, ax):

		(r1, r2) = self.get_radial_vectors()
		#Draw first radial vector
		(o1x, o1y) = self.top_link.get_end_point()
		(l1x, l1y) = self.top_link.get_CoM()

		ax.plot([o1x-l1x, o1x - l1x+r1[0]], [o1y - l1y, o1y - l1y + r1[1]], 'red')

		(o2x, o2y) = self.bottom_link.get_end_point()
		(l2x, l2y) = self.bottom_link.get_CoM()

		ax.plot([o2x-l2x, o2x-l2x + r2[0]], [o2y - l2y, o2y - l2y + r2[1]], 'red')


	def draw(self, ax):
		#Draws the spring
		top_angle = self.top_link.angle * pi/180.
		bottom_angle = self.bottom_link.angle * pi/180.
		

		r1 = self.top_link.get_end_point()
		ex = [r1[0] + self.extension * sin(top_angle), r1[1] - self.extension * cos(top_angle)]
		r2 = [r1[0] + (self.bottom_link.separation * sin(bottom_angle)), r1[1] - (self.bottom_link.separation * cos(bottom_angle))]

		#Draw spring

		ax.plot([ex[0], r2[0]], [ex[1], r2[1]], 'blue')


def test(angle):

	l1 = link.Link()
	l2 = link.Link()
	l3 = link.Link()
	l4 = link.Link()

	l2.angle = angle
	l3.angle = 2 * angle
	l4.angle = 3 * angle


	l1.child = l2
	l2.parent = l1

	l2.child = l3
	l3.parent = l2

	l3.child = l4
	l4.parent = l3

	fig = plt.figure()
	ax = fig.add_subplot(111)

	l1.draw(ax)
	l2.draw(ax)
	l3.draw(ax)
	l4.draw(ax)

	s1 = MACEPPA(l1, l2, 15, 0.05, .01)
	s2 = MACEPPA(l2, l3, 15, 0.05, .01)
	s3 = MACEPPA(l3, l4, 15, 0.05, .01)

	s1.draw(ax)
	s2.draw(ax)
	s3.draw(ax)

	s1.draw_force_vectors(ax)
	s1.draw_radial_vectors(ax)
	s2.draw_force_vectors(ax)
	s2.draw_radial_vectors(ax)
	s3.draw_force_vectors(ax)
	s3.draw_radial_vectors(ax)

	print s1.get_length()
	print s2.get_length()
	print s3.get_length()

	print s1.get_force()
	print s2.get_force()
	print s3.get_force()

	print s1.get_torque()
	print s2.get_torque()
	print s3.get_torque()

	plt.show()

def plot_maceppa():

	l1 = link.Link()
	l2 = link.Link()


	l1.child = l2
	l2.parent = l1


	s1 = MACEPPA(l1, l2, 1500, 0.02, 0.0)

	for p in [0.0, 0.01, 0.02, 0.05, 0.1]:
		s1.pretension = p
		ang = -90
		step = 180/100.
		torques = []
		angs = []
		for i in range(100):
			l2.angle = ang
			t = s1.get_torque()
			torques.append(t[0])
			angs.append(ang)
			ang += step

		plt.plot(angs, torques)

	plt.show()









