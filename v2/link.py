import numpy as np 
import sympy as sp
import matplotlib
import matplotlib.pyplot as plt 
import seaborn as sn
from numpy import sin, cos

'''
This class stores properties and convenience methods for the individual links that make up a snake
'''

class Link:

	def __init__(self, params=dict()):
		#Default parameters
		self.mass = .4
		self.width = 0.05
		self.length = 0.4
		self.separation = 0.1
		self.angle = 0.0
		self.parent = None
		self.child = None
		self.name = "None"

		#Overwrite defaults if params is specified
		for (k, v) in params.iteritems():
			setattr(self, k, v)

		#Calculate derived parameters
		self.spring = None

	def get_CoM(self):
		#Returns the CoM of the link within its own reference frame

		angle = self.angle * 2 * np.pi/360

		x = (self.separation + self.length / 2.0) * np.sin(angle)
		y = -(self.separation + self.length / 2.0) * np.cos(angle)

		return (x, y)

	def get_gravity_force(self, parent_height=0.0):
		#Returns the force required to maintain the current position of this link and ALL child links

		angle = self.angle * 2 * np.pi/360

		if not self.child:
			return ((self.length/2.) + self.separation) * self.mass * (((self.length/2.)+self.separation)*(1-cos(angle))+parent_height)
		else:
			end_height = (self.length + (2*self.separation))*(1-cos(angle))
			return ((self.length/2.) + self.separation) * self.mass * ((((self.length/2.) + self.separation) * (1-cos(angle)))+parent_height) + self.child.get_gravity_force(end_height)

	def get_corner(self):
		#Returns the corner used to draw the link rectangle
		angle = self.angle * 2 * np.pi/360

		x1 = self.separation * np.sin(angle) + (self.width / 2.0) * np.cos(angle)
		y1 = -self.separation * np.cos(angle) + (self.width / 2.0) * np.sin(angle)

		return (x1, y1)

	def get_end_point(self):

		#Returns the end point of the link in the GLOBAL reference frame
		if self.parent:
			(x0, y0) = self.parent.get_end_point()
		else:
			(x0, y0) = (0,0)

		angle = self.angle * 2 * np.pi/360

		x = np.sin(angle) * (self.length + (self.separation*2))
		y = -np.cos(angle) * (self.length + (self.separation*2))

		return (x0+x, y0+y)

	def get_radial_vectors(self):
		#Returns the vectors from the CoM to each of the corners
		#This is where the spring forces are applied to the link

		angle = self.angle * 2 * np.pi/360
		#Bottom left
		r1x = (-self.width/2.) * cos(angle) + (self.length/2) * sin(angle)
		r1y = (-self.width/2.) * sin(angle) - (self.length/2) * cos(angle)
		self.r1 = [r1x, r1y]

		#Bottom right
		r2x = (self.width/2.) * cos(angle) + (self.length/2) * sin(angle)
		r2y = (self.width/2.) * sin(angle) - (self.length/2) * cos(angle)
		self.r2 = [r2x, r2y]

		#Top right
		r3x = (self.width/2.) * cos(angle) - (self.length/2) * sin(angle)
		r3y = (self.width/2.) * sin(angle) + (self.length/2) * cos(angle)
		self.r3 = [r3x, r3y]

		#Top left
		r4x = (-self.width/2.) * cos(angle) - (self.length/2) * sin(angle)
		r4y = (-self.width/2.) * sin(angle) + (self.length/2) * cos(angle)
		self.r4 = [r4x, r4y]

		return [self.r1, self.r2, self.r3, self.r4]

	def get_corners(self):
		#Returns the positions of the link corners in the GLOBAL reference frame

		if self.parent:
			origin = self.parent.get_end_point()
		else:
			origin = (0, 0)

		CoM = self.get_CoM()
		rs = self.get_radial_vectors()

		r1 = [origin[0] + CoM[0] + rs[0][0], origin[1] + CoM[1] + rs[0][1]]
		r2 = [origin[0] + CoM[0] + rs[1][0], origin[1] + CoM[1] + rs[1][1]]
		r3 = [origin[0] + CoM[0] + rs[2][0], origin[1] + CoM[1] + rs[2][1]]
		r4 = [origin[0] + CoM[0] + rs[3][0], origin[1] + CoM[1] + rs[3][1]]

		return [r1, r2, r3, r4]

	def plot_radial_vectors(self, ax):

		#Shows the four radial vectors -- used for debugging
		if self.parent:
			origin = self.parent.get_end_point()
		else:
			origin = (0, 0)

		rs = self.get_radial_vectors()
		CoM = self.get_CoM()

		ax.plot([origin[0] + CoM[0], origin[0] + CoM[0] + rs[0][0]], [origin[1] + CoM[1], origin[1] + CoM[1] + rs[0][1]], 'green')
		ax.plot([origin[0] + CoM[0], origin[0] + CoM[0] + rs[1][0]], [origin[1] + CoM[1], origin[1] + CoM[1] + rs[1][1]], 'green')
		ax.plot([origin[0] + CoM[0], origin[0] + CoM[0] + rs[2][0]], [origin[1] + CoM[1], origin[1] + CoM[1] + rs[2][1]], 'green')
		ax.plot([origin[0] + CoM[0], origin[0] + CoM[0] + rs[3][0]], [origin[1] + CoM[1], origin[1] + CoM[1] + rs[3][1]], 'green')


	def get_plot_data(self):

		if self.parent:
			(x0, y0) = self.parent.get_end_point()
		else:
			(x0, y0) = (0, 0)

		(xr, yr) = self.get_corner()

		rect = matplotlib.patches.Rectangle((x0 + xr, y0 + yr), self.width, self.length, self.angle-180.0)

		angle = self.angle * 2 * np.pi/360

		#Draw a line from the joint to the link
		x1 = x0
		y1 = y0
		x2 = x0 + np.sin(angle) * self.separation
		y2 = y0 -np.cos(angle) * self.separation

		#Draw a line from the link to the next joint
		x3 = x0 + np.sin(angle) * (self.separation + self.length)
		y3 = y0 -np.cos(angle) * (self.separation + self.length)
		x4 = x0 + np.sin(angle) * (2*self.separation + self.length)
		y4 = y0 -np.cos(angle) * (2*self.separation + self.length)

		return (rect, (x1, x2), (y1, y2), (x3, x4), (y3, y4))

	def draw(self, ax):

		#Draw the link first as matplotlib likes angles in degrees while numpy wants radians
		(rect, (x1, x2), (y1, y2), (x3, x4), (y3, y4)) = self.get_plot_data()

		ax.add_patch(rect)
		ax.plot([x1, x2], [y1, y2], 'black')
		ax.plot([x3, x4], [y3, y4], 'black')

def test(angle):

	fig = plt.figure()
	ax = fig.add_subplot(111)
	L = Link()
	L.angle = angle
	L.draw(ax)
	L.plot_radial_vectors(ax)
	plt.show()





