from scipy.integrate import odeint
from numpy import ones, sin, cos, linspace, zeros, array, hstack, zeros, linspace, pi
from numpy.linalg import solve
import numpy as np
from sympy import Dummy, lambdify
from sympy import symbols
from sympy.physics.mechanics import *
import random
import copy
import scipy.signal
import matplotlib
matplotlib.use('TKAgg')
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import cm
import seaborn as sn
plt.ion()
import link
import motor
import spring
import rk4
import scipy.signal
'''
Solves n-link pendulum with spring forces
Returns equations of motion
Input: # of links
'''

class Snake:
    def __init__(self, n=2, params=dict()):

        #Default paramaters
        self.n = n
        self.links = [link.Link() for _ in range(n)]
        self.friction = [-0.1 for _ in range(n)]
        self.spring_type = spring.Spring
        self.gravity = -9.81

        self.int_method = rk4.rk4
        self.t = []

        #Attach springs to links
        self.springs = []
        for i in range(len(self.links)-1):
            #Springs require the parent link first, child link second.
            #s = self.spring_type(self.links[i], self.links[i+1], 200000 - (i * 20000))
            s = spring.MACEPPA(self.links[i], self.links[i+1], 1500, 0.02, 0.00)
            self.springs.append(s)

            #Setup link numbers
            self.links[i].name = str(i)

        self.links[i+1].name = str(i+1)

        #Attach motors to joints
        #self.motors = [motor.Const(0., self.links[0])]
        self.motors = [motor.PID(sin, 0.2, 15, 30., 2000., self.links[0])]
        for i in range(len(self.links)-1):
            #Motors require the child link first, parent link second.
            m = motor.Const(0, self.links[i+1], self.links[i])
            self.motors.append(m)

        #overwrite defaults
        for (k, v) in params.iteritems():
            setattr(self, k, v)

        #Calculate derived paramaters
        #Add parent/child information for each link
        if n > 1:
            for (i, l) in enumerate(self.links):
                if i == 0:
                    l.child = self.links[i+1]
                elif i == self.n-1:
                    l.parent = self.links[i-1]
                else:
                    l.child = self.links[i+1]
                    l.parent = self.links[i-1]

    def get_eom(self):

        self.q = dynamicsymbols('q:' + str(self.n)) #Generalised coordinates
        self.qd = dynamicsymbols('qd:' + str(self.n)) #Generalised velocities
        self.k = dynamicsymbols('k:' + str(self.n)) #Spring Torques
        self.u = dynamicsymbols('u:' + str(self.n)) #Motor Forces
        self.fric = dynamicsymbols('fric:' + str(self.n)) #Frictional forces
        self.m = symbols('m:' + str(self.n)) #Mass of each segment
        self.l = symbols('l:' + str(self.n)) #Length of each segment
        self.w = symbols('w:' + str(self.n)) #Width of each segment
        self.s = symbols('s:' + str(self.n)) #Seperation of each segment
        self.g, self.t = symbols('g t')   #Gravity and time

        #Create a reference frame and origin
        N = ReferenceFrame('N')
        O = Point('O')
        O.set_vel(N, 0)

        frames = [N]
        points = []
        particles = []
        forces = []
        kindiffs = []

        for i in range(self.n):
            #Create a reference frame for the link
            Bi = N.orientnew('B' + str(i), 'Axis', [-self.q[i], N.z])
            Bi.set_ang_vel(N, -self.qd[i] * N.z)
            frames.append(Bi)

            #Create a point for the link
            if not points:
                #Each link has its CoM at the distance seperation + length/2.0 from its parent link
                Pi = O.locatenew('P' + str(i), -(self.s[i] + (self.l[i]/2.0)) * Bi.y)
                Pi.v2pt_theory(O, N, Bi)
                points.append(Pi)

            else:
                Pi = points[-1].locatenew('P' + str(i), -((self.s[i-1]+(self.l[i-1]/2.0)) * frames[-1].y)-((self.l[i]/2.0)+self.s[i]) * Bi.y)
                Pi.v2pt_theory(points[-1], N, Bi)
                points.append(Pi)

            #Add mass to the point
            Iix = self.l[i]**3 * self.w[i] * (1./12)
            Iiy = self.l[i] * self.w[i]**3 * (1./12)
            Iiz = self.l[i] * self.w[i] * (self.l[i]**2+self.w[i]**2) * (1./12)
            Ii = inertia(Bi, Iix, Iiy, Iiz)
            Pai = RigidBody('Pa' + str(i), Pi, Bi, self.m[i], (Ii, Pi))
            particles.append(Pai)

            forces.append((Pi, (Bi.x * self.k[i]))) #Spring Torques
            forces.append((Pi, (self.m[i] * self.g * N.y))) #Gravity Force
            forces.append((Pi, (Bi.x * self.u[i]))) #Motor Torque 
            forces.append((Pi,  (Bi.x * self.fric[i]))) #Joint Friction
            

            kindiffs.append(self.q[i].diff(self.t) - self.qd[i])

        self.KM = KanesMethod(N, q_ind=self.q, u_ind=self.qd, kd_eqs=kindiffs)
        (self.fr, self.frstar) = self.KM.kanes_equations(forces, particles)

    def sub_params(self):

        parameters = [self.g]                       # Parameter definitions starting with gravity and the first bob
        self.parameter_vals = [self.gravity]

        for i in range(self.n):                           # Then each mass and length
            parameters += [self.l[i], self.m[i], self.s[i], self.w[i]]          
            self.parameter_vals += [self.links[i].length, self.links[i].mass, self.links[i].separation, self.links[i].width]

        self.dynamic = self.q + self.qd + self.k + self.u + self.fric
        dummy_symbols = [Dummy() for i in self.dynamic]
        dummy_dict = dict(zip(self.dynamic, dummy_symbols))
        self.kindiff_dict = self.KM.kindiffdict()

        self.M = self.KM.mass_matrix_full.subs(self.kindiff_dict).subs(dummy_dict)  # Substitute into the mass matrix 
        self.F = self.KM.forcing_full.subs(self.kindiff_dict).subs(dummy_dict)      # Substitute into the forcing vector

        self.M_func = lambdify(dummy_symbols + parameters, self.M)               # Create a callable function to evaluate the mass matrix 
        self.F_func = lambdify(dummy_symbols + parameters, self.F)

        return (self.M_func, self.F_func)

    def make_rhs(self):

        #Get the equations of motion and substitute parameters into expression

        #Create a function for the derivative of the system
        def right_hand_side(x, t, args):

            #Update link angles
            for (i, l) in enumerate(self.links):
                l.angle = x[i]

            k = [0.0 for _ in range(self.n)]

            for (i, s) in enumerate(self.springs):
                (t1, t2) = s.get_torque()
                k[i] += t1
                k[i+1] += t2

            #Motor torques acting on each link
            u = [m.activate(x, t) for m in self.motors]

            fr = [-self.friction[i] * x[self.n]] + [-self.friction[i] * (x[i+self.n+1] - x[i+self.n]) for i in range(self.n-1)]

            forces = k + u + fr

            arguments = hstack((x, forces, args))     # States, input, and parameters
            dx = array(solve(self.M_func(*arguments), # Solving for the derivatives
                self.F_func(*arguments))).T[0]

            #Overwrite first link terms here if position controlled motor?

            return dx

        self.rhs = right_hand_side

    def solve(self, x0, t):
        self.make_rhs()
        self.t = t
        self.y = self.int_method(self.rhs, x0, t, args=(self.parameter_vals,))

    def plot_state(self, fig=None):

        if not fig:
            fig = plt.figure()
            own_fig = True
            fig.canvas.set_window_title("Absolute State Space")
        else:
            own_fig = False
            fig.clear()

        for _ in range(self.n):
            q_ax = fig.add_subplot(self.n, 2, _+1)
            qd_ax = fig.add_subplot(self.n, 2, _ + 1 + self.n) 

            q_ax.plot(self.t, self.y[:, _])
            q_ax.set_xlabel('Time [s]')
            q_ax.legend([self.dynamic[_]])

            qd_ax.plot(self.t, self.y[:, _+ self.n])
            qd_ax.set_xlabel('Time [s]')
            qd_ax.legend([self.dynamic[_+self.n]])

        if own_fig:
            plt.show()
        else:
            fig.canvas.draw()

    def plot_relative_state(self, fig=None):

        if not fig:
            fig = plt.figure()
            own_fig = True
            fig.canvas.set_window_title("Relative State Space")
        else:
            own_fig = False
            fig.clear()

        q_ax = fig.add_subplot(self.n, 2, 1)
        qd_ax = fig.add_subplot(self.n, 2, 1 + self.n)

        q_ax.plot(self.t, self.y[:, 0])
        q_ax.set_xlabel('Time [s]')
        q_ax.legend([self.dynamic[0]])

        qd_ax.plot(self.t, self.y[:, self.n])
        qd_ax.set_xlabel('Time [s]')
        qd_ax.legend([self.dynamic[self.n]])

        for _ in range(self.n-1):
            q_ax = fig.add_subplot(self.n, 2, _+2)
            qd_ax = fig.add_subplot(self.n, 2, _ + 2 + self.n) 

            q_ax.plot(self.t, self.y[:, _+1]-self.y[:, _])
            q_ax.set_xlabel('Time [s]')
            q_ax.legend([self.dynamic[_+1]])

            qd_ax.plot(self.t, self.y[:, _+1+self.n]-self.y[:, _+ self.n])
            qd_ax.set_xlabel('Time [s]')
            qd_ax.legend([self.dynamic[_+1+self.n]])

        if own_fig:
            plt.show()
        else:
            fig.canvas.draw()

    def plot_phase(self, fig=None):

        if not fig:
            fig = plt.figure()
            own_fig = True
            fig.canvas.set_window_title("Phase Space")
        else:
            own_fig = False
            fig.clear()

        q_ax = fig.add_subplot(self.n, 1, 1)

        q_ax.plot(self.y[:, 0], self.y[:, self.n])
        q_ax.legend([self.dynamic[0]])

        for _ in range(self.n-1):
            q_ax = fig.add_subplot(self.n, 1, _+2)
            q_ax.plot(self.y[:, _+1], self.y[:, _+1+self.n])
            q_ax.legend([self.dynamic[_+1]])

        if own_fig:
            plt.show()
        else:
            fig.canvas.draw()

    def plot_fft(self, fig=None):

        if not fig:
            fig = plt.figure()
            own_fig = True
            fig.canvas.set_window_title("FFT Space")
        else:
            own_fig = False
            fig.clear()

        for _ in range(self.n):

            n = len(self.y[:, _]) # length of the signal
            k = np.arange(n)
            T = 4000/20.
            frq = k/T # two sides frequency range
            frq = frq[range(n/2)] # one side frequency range

            Y = np.fft.fft(self.y[:, _])/n # fft computing and normalization
            Y = Y[range(n/2)]

            q_ax = fig.add_subplot(self.n, 2, _+1)
            qd_ax = fig.add_subplot(self.n, 2, _ + 1 + self.n) 

            q_ax.plot(frq,abs(Y),'r')
            q_ax.set_xlabel('Freq [Hz]')
            q_ax.legend([self.dynamic[_]])

            n = len(self.y[:, _+self.n]) # length of the signal
            k = np.arange(n)
            T = 4000/20.
            frq = k/T # two sides frequency range
            frq = frq[range(n/2)] # one side frequency range

            Y = np.fft.fft(self.y[:, _+self.n])/n # fft computing and normalization
            Y = Y[range(n/2)]

            qd_ax.plot(frq,abs(Y),'r')
            qd_ax.set_xlabel('Freq [Hz]')
            qd_ax.legend([self.dynamic[_+self.n]])

        if own_fig:
            plt.show()
        else:
            fig.canvas.draw()


    def plot_snake(self, t_index, fig=None):
        
        if not fig:
            fig = plt.figure()
            own_fig = True
        else:
            own_fig = False
            fig.clear()
    
        # set the limits based on the lengths
        ax_lim = np.sum([l.length + (2*l.separation) for l in self.links])*1.1
        # create the axes
        self.ax = fig.add_subplot(111, aspect='equal', autoscale_on=False, xlim=[-ax_lim, ax_lim], ylim=[-ax_lim, ax_lim])

        for (i, l) in enumerate(self.links):
            l.angle = self.y[t_index][i] * 360. / (2 * pi)
            l.draw(self.ax)


        self.springs[-1].draw(self.ax)
        self.springs[-1].draw_force_vectors(self.ax)
        self.springs[-1].draw_radial_vectors(self.ax)

        print self.springs[-1].get_force()
        print self.springs[-1].get_torque()
    
        if own_fig:
            plt.show()
        else:
            fig.canvas.draw()

    def animate_snake(self, num_frames=None, filename=None):

        self.fig = plt.figure()

        self.anim = animation.FuncAnimation(self.fig, self.animate, frames=num_frames, init_func=self.init_animation,
            interval=0.1, blit=True, repeat=True)

        plt.show()

       
        # save the animation if a filename is given
    # initialization function: plot the background of each frame
    def init_animation(self):

        # set the limits based on the lengths
        ax_lim = np.sum([l.length + (2*l.separation) for l in self.links])*1.1
        # create the axes
        self.ax = self.fig.add_subplot(111, aspect='equal', autoscale_on=False, xlim=[-ax_lim, ax_lim], ylim=[-ax_lim, ax_lim])
        
        # display the current time
        self.time_text = self.ax.text(0.04, 0.9, '', transform=self.ax.transAxes)
        self.ang_text = self.ax.text(0.04, 0.85, '', transform=self.ax.transAxes)
        
        # blank line for the pendulum
        self.line, = self.ax.plot([], [], lw=2, marker='o', markersize=1)
        #blank line for the path of the final link
        self.path, = self.ax.plot([], [], lw=1, marker='+', markersize=2)

        #Store the last (500) (x,y) positions of the end mass
        self.final_xs = []
        self.final_ys = []


        self.time_text.set_text('')
        self.ang_text.set_text('')
        self.line.set_data([], [])
        self.path.set_data([], [])
        self.final_xs = []
        self.final_ys = []


        return self.time_text, self.ang_text, self.line, self.path, self.final_xs, self.final_ys

    # animation function: update the objects
    def animate(self, i):
        i*= 10
        #Update time and angle texts
        self.time_text.set_text('time = {:2.2f}'.format(self.t[i]))
        self.ang_text.set_text('ang = {:2.2f}'.format(self.y[i, 0]))

        springs = []
        rects = []
        x = []
        y = []

        #Draw each link
        for (j, l) in enumerate(self.links):
            l.angle = self.y[i][j] * 360. / (2 * pi)
            (rect, (x1, x2), (y1, y2), (x3, x4), (y3, y4)) = l.get_plot_data()
            self.ax.add_patch(rect)
            x+= [x1, x2, x3, x4]
            y+=[y1, y2, y3, y4]
            rects.append(rect)

        #Store the positions of the last mass
        if len(self.final_xs) < 500:
            self.final_xs.append(x[-1])
            self.final_ys.append(y[-1])
        else:
            self.final_xs.pop(0)
            self.final_ys.pop(0)
            self.final_xs.append(x[-1])
            self.final_ys.append(y[-1])

        #Update line and path objects
        self.line.set_data(x, y)
        self.path.set_data(self.final_xs, self.final_ys)

        #Lists to store block and spring objects
        return (self.time_text, self.ang_text, self.line, self.path,) + tuple(rects) + tuple(springs)

def main():
    N=4
    s = Snake(N)
    s.get_eom()
    s.sub_params()
    s.make_rhs()
    x0 = hstack(([ 0.5 - (_ * 0.1) for _ in range(N)], zeros(N))) # Initial conditions, q and u
    time = linspace(0, 50, 50000)                         # Time vector
    s.solve(x0, time)
    s.plot_state()
    s.plot_relative_state()
    s.plot_phase()
    s.plot_fft()
    s.animate_snake(len(time)/10)
    raw_input()

if __name__ == "__main__":
    main()







