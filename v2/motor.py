import scipy.signal
import numpy as np 
import matplotlib.pyplot as plt 

'''
This class represents a motor + control policy
It stores properties and convenience methods
A motor is attached at the point two links join.
'''

class Motor:

	def __init__(self, child, parent=None, params=dict()):

		self.umax = 1000.0
		#Child represent the link actuated by the motor
		self.child = child

		#Parent represents the link attached to the motor. The motor rotates with the link.
		self.parent = parent

		for (k, v) in params.iteritems():
			setattr(self, k, v)

	def saturate(self, output):
		#Apply saturation limits
		return max(min(output, self.umax), -self.umax)

	def activate(self, state, t):
		#Return the torque determined by the control policy
		#Policy method must be implemented by the various motor subclasses
		output = self.policy(state, t)

		return self.saturate(output)


class Const(Motor):

	def __init__(self, value, child, parent=None, params=dict()):

		self.value = value
		self.type = "Const"
		Motor.__init__(self, child, parent, params)

	def policy(self, state, t):

		return self.value


class PID(Motor):

	def __init__(self, reference, amplitude, frequency, kp, kd, child, parent=None, params=dict()):

		self.type = "PID"
		self.reference = reference
		self.kd = kd
		self.kp = kp
		self.amplitude = amplitude
		self.frequency = frequency
		Motor.__init__(self, child, parent, params)

		self.last_error = 0.0

	def policy(self, state, t):

		self.set_point = self.amplitude * self.reference(self.frequency*t)

		if self.parent:
			self.point = self.child.angle - self.parent.angle
		else:
			self.point = self.child.angle

		error = self.point - self.set_point
		error_d = error - self.last_error


		output = (self.kp * error) + (self.kd * error_d) + (9.81 * self.child.get_gravity_force())
		self.last_error = error

		return output







